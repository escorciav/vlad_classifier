# VLAD Classifier
This repository contains code to run a pre-trained C3D+VLAD+SVM classifier.

A pre-trained model in Thumos14 is included (data/models):

* Codebook Size: 256
* VLAD: L2 Normalization
* SVM: 1 vs All (C=100)

Please look at the helper in the file ```classify_segments.py``` to score 
segments with action confidences.
