import argparse
import os
import pickle
import shutil

import numpy as np
import pandas as pd

from utils import call_vlad_batch_encoding
from utils import dump_segments
from utils import load_feature_matrix

def main(dataset_info, feature_file, kmeans_model, svm_model,
         detector_data_path, video_name, mdriver, vlfeat):
    """ Score a batch of segments from an inteded video (one video at a time).

    Params
    ------
    dataset_info : str
        path to csv file containing the info about the temporal
        segments to score.
    video_name : str
        video identifier.
    feature_file : str
        path to hdf5 file containing the raw C3D features.
    kmeans_model : str
        path to hdf5 file containing the K-Means model.
    svm_model : str
        path to pickle file containing an SKLearn SVM model object.
    detector_data_path : str
        path where all data generated by this script will be stored.
    """

    ############################################################################
    # Defining output filenames.
    ############################################################################
    scoring_filename = os.path.join(detector_data_path,
                                    'scores', '{}.csv' .format(video_name))
    # Avoid over-writting and warranty the output will be written.
    if os.path.exists(scoring_filename):
        raise IOError('Output filename {} exists.').format(scoring_filename)
    scores_dir = os.path.join(detector_data_path, 'scores')
    if not os.path.exists(scores_dir):
        os.makedirs(scores_dir)

    ############################################################################
    # Feature extraction (Calling alien application - Matlab)
    ############################################################################
    # Dumps C3D temporary on disk.
    df = pd.read_csv(dataset_info, sep=' ')
    idx = df['video-name'] == video_name
    df = df.loc[idx].copy()
    segments_dir = os.path.join(detector_data_path,
                                'features', 'c3d', video_name)
    if not os.path.isdir(segments_dir):
        os.makedirs(segments_dir)
    segment_info = os.path.join(detector_data_path,
                                'info', '{}.csv'.format(video_name))
    if not os.path.isdir(os.path.dirname(segment_info)):
        os.makedirs(os.path.dirname(segment_info))
    dump_segments(df, feature_file, segments_dir, segment_info)

    # A Matlab application is called to encode the batch of C3D features.
    vlad_dir = os.path.join(detector_data_path, 'features', 'vlad', video_name)
    if not os.path.isdir(vlad_dir):
        os.makedirs(vlad_dir)
    call_vlad_batch_encoding(kmeans_model, segments_dir, 
                             vlad_dir, mdriver, vlfeat)

    ###########################################################################
    # Segment scoring
    ###########################################################################
    df = pd.read_csv(segment_info, sep=' ')
    # Loading proposals features.
    X = load_feature_matrix(df, vlad_dir)
    # Loading models.
    with open(svm_model, 'rb') as fobj:
         model_dict = pickle.load(fobj)
    label_lst = model_dict['config']['classes']
    label_lst = [l for l in label_lst if l >= 0] # Skipping "background class".
    # Scoring samples.
    n_samples = X.shape[0]
    n_classes = len(label_lst)
    scores = np.zeros((n_samples, n_classes))
    for i, label in enumerate(label_lst):
        model = model_dict['models'][label]
        scores[:, i] = model.decision_function(X)
    # Build DataFrame.
    dd = {}
    for i, label in enumerate(label_lst):
        dd['score_class_{}'.format(int(label))] = scores[:, i]
    sc_df = pd.DataFrame(dd)
    result_df = pd.concat([df, sc_df], axis=1)
    result_df.to_csv(scoring_filename, sep=' ', index=False)
    # Remove temporary data.
    shutil.rmtree(segments_dir)
    shutil.rmtree(vlad_dir)

if __name__ == '__main__':
    description = 'Score temporal segments using a pre-trained SVM model.'
    p = argparse.ArgumentParser(description=description)
    p.add_argument('dataset_info',
                   help='csv file with the temporal segments.')
    p.add_argument('feature_file',
                   help='hdf5 file containing the raw C3D features.')
    p.add_argument('kmeans_model',
                   help='hdf5 file containing the K-Means model.')
    p.add_argument('svm_model',
                   help='pickle file containing the SVM model.')
    p.add_argument('detector_data_path',
                   help='Where to dump all the data related with the detection.')
    p.add_argument('video_name',
                   help='Video identifier which will be processed.')
    p.add_argument('--mdriver', help='Path to matlab functions (VLAD)',
                   default=os.path.join(os.path.dirname(__file__), 'matlab'))
    p.add_argument('--vlfeat', help='Path to VLFeat toolbox.',
                   default=os.path.join(os.path.dirname(__file__), 
                                        'matlab/vlfeat/toolbox'))
    main(**vars(p.parse_args()))
