import argparse
import glob
import os

MATLAB_DRIVER = ''
VLFEAT_APP = ''

def call_vlad_encoding(kmeans_model, filename, output_filename, 
                       mdriver=MATLAB_DRIVER, vlfeat=VLFEAT_APP):
    cmd = ("matlab -r \"try addpath('{}'); vl_setup; addpath('{}'); "
           "vlad_segment_encoding('{}', '{}', '{}'); catch; end; quit\" >> "
           "/tmp/running.tmp".format(vlfeat, mdriver, kmeans_model, 
                                     filename, output_filename)
    result = os.system(cmd)

def main(dataset_path, kmeans_model, output_path):
    ptrn = os.path.join(dataset_path, '*.hdf5')
    files = sorted(glob.glob(ptrn))
    kmeans_model = os.path.abspath(kmeans_model)
    for idx, f in enumerate(files):
        bname = os.path.basename(f)
        output_filename = os.path.abspath(os.path.join(output_path, bname))
        if os.path.exists(output_filename):
            continue
        filename = os.path.abspath(f)
        call_vlad_encoding(kmeans_model, filename, output_filename)
        if (idx % 100) == 0:
            print 'Processed: {}/{}'.format(idx, len(files))


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('dataset_path',
                   help='Path where the hdf5 c3d files are located.')
    p.add_argument('kmeans_model',
                   help='hdf5 file with the kmeans model.')
    p.add_argument('output_path',
                   help='where to locate vlad features.')
    main(**vars(p.parse_args()))
