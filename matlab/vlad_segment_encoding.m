function enc = vlad_segment_encoding(kmeans_model, c3d_feat_file, output_filename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Encodes a C3D file from a segment using VLAD.
% Note: Requires VLFeat Toolbox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reads K-Means model from hdf5 file.
centers = hdf5read(kmeans_model, '/data/cluster_centers_');
[dummy, n_clusters] = size(centers);
% Reads C3D feature from file.
data_to_encode = hdf5read(c3d_feat_file, '/data');
[dummy, n_to_encode] = size(data_to_encode);
% Builds kdtree to speed up vlad.
kdtree = vl_kdtreebuild(centers);
nn = double(vl_kdtreequery(kdtree, centers, data_to_encode));
% Computes VLAD.
assignments = zeros(n_clusters, n_to_encode);
assignments(sub2ind(size(assignments), nn, 1:length(nn))) = 1;
enc = vl_vlad(data_to_encode, centers, assignments, 'SquareRoot');
% Save results in a hdf5 file.
hdf5write(output_filename, '/data', enc);
