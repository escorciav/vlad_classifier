#!/usr/bin/env python
import argparse
import os

import h5py
import pandas as pd

from c3d_feature_helper import Feature

def main(input_file, feature_file, dataset_path, output_filename,
         fname_format='{0:04d}'):
    # Read annotations.
    df = pd.read_csv(input_file, sep=' ')
    fname_lst = []
    feat_obj = Feature(feature_file, pool_type=None)
    feat_obj.open_instance()
    true_idx = []
    cnt_i = 0
    for idx, v in df.iterrows():
        this_filename = os.path.join(dataset_path,
                                     '{}.hdf5'.format(fname_format.format(idx)))
        try:
            feat = feat_obj.read_feat(v['video-name'], v['f-init'], v['n-frames'])
            f = h5py.File(this_filename, 'w')
            f.create_dataset('data', data=feat)
            fname_lst.append(fname_format.format(idx))
            true_idx.append(idx)
            f.close()
        except:
            print v, '\n'
            cnt_i += 1
    dset = pd.DataFrame({'video-name': df['video-name'].iloc[true_idx],
                         'filename': fname_lst,
                         'f-init': df['f-init'].iloc[true_idx],
                         'n-frames': df['n-frames'].iloc[true_idx],
                         'label-idx': df['label-idx'].iloc[true_idx]})
    dset.to_csv(output_filename, sep=' ', index=False)
    print "Skipped segments: {}".format(cnt_i)

if __name__ == '__main__':
    dsc = 'Save C3D features for positive segments in individual files.'
    p = argparse.ArgumentParser(description=dsc)
    p.add_argument('input_file',
                   help='csv file containing segment annotations')
    p.add_argument('feature_file',
                   help='hdf5 file containing the C3D features.')
    p.add_argument('dataset_path',
                   help='where to store the dataset.')
    p.add_argument('output_filename',
                   help='dataset csv')
    main(**vars(p.parse_args()))
